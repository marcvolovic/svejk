#set par(justify: true)

// Title page function
//
#let title-page(title, author) = {
  set text(size:16pt)
}

// Dictionary entry function
//
#let addentry(entry, type, body) = {
  [ #strong[#entry] #emph[(#type)] #sym.circle.filled #body ]
}

///////////////////////////////////////////////////////

#outline()

// Dictionary starts here
//
= Preface

This work was conceived in the spring of 2021, as COVID-19 was ostensibly
receding in Israel. Nature does not love vacuum, so COVID was replaced by a
government stupidity increase. To make this increase unforgettable, the post of
the Minister of Education was given to an ex-general of a such Zillergutesque
ability that a seed of an idea was planted in my mind.

This seed was given an invigorating kick by a chance conversation with a
coworker. "As it is written at the very end of The Brave Soldier Shveik (thus
is the book called in Hebrew)," said he, "we'll meet at six as the war
is ending."

A shudder went through me and the seed sprouted right there and then. Or, if
truth be known, it did so after a minor application of a small bottle of port
for mental lubrication. I shall, it was decided, write an almanac for my peers,
listing the important people in the book. The main characters. And also, the
famous quotes. And, maybe, the less important people.  Also - curses. And the
animals. Oh, vehicles. Food. Drink. In short, anything that catches a magpie's
fancy.

Errors and omissions are mine, your worships, and are both the result and the
cause of a somewhat liberal application of a simple home-made remedy for cold,
rheumatism, general distempter and misanthropy. And, therefore, entirely mine.

= Note On Notation And Orthography

The edition used for the compilation of this work is the Harper Perennial
(originally the 1973 William Heinemann/Penguin Books), translated by Cecil
Parrott. Another was a slightly later imprint, with page numbers almost exactly
the same.

Initially, I wanted to use page numbers for place markers and references. But
using two different editions of the book dissuaded me. Thus, page numbers are
not used. Instead, place markers take the form of P#emph[x]C#emph[y] where _x_
stands for part and _y_ stands for chapter, making P5C66 reference "Part 5,
Chapter 66". Many, if not all, entries in the almanac are accompanied by place
markers, with the exception of Švejk, for obvious enough reasons.

I tried to follow the spelling of various Czech and German words and placenames
where and when I could.


#set page(columns: 2)

= Almanac

== 1-9

#addentry[11,000#super[th] Man][Person][
  A civilian examined by Dr. Bautze of the medical board. Dies of a stroke
  during the examination and is, thus, not officially recognised for being the
  malingerer that he was (P1C7).
]

#addentry[1871][Year][
  France has a dislike of Germany since 1871, the year of the War of German
  Unification (P1C1).
]

#addentry[28#super[th] Regiment][Organisation][
  Otto Katz' ministrant is a deserter from the 28#super[th] regiment
  (P1C9).
]

#addentry[7267][Print][
  Charge No. 7267 is brought against Private Maixner and materials pertaining
  to it are acknowledged by Captain Linhart, instead of by the provost marshal
  (P1C9).
]

#addentry[75#super[th] Regiment][Organisation][
  The 75#super[th] infantry regiment of the Imperial army. Švejk is
  mistaken for its commander, Colonel Just, by the drunk Otto Katz (P1C10).
  Lt. Wittinger serves in the 75#super[th].
]

#addentry[91#super[st] Regiment][Organisation][
  The 91#super[st] regiment of the Imperial army (P1C6). A captain the
  91#super[st] used to tell the soldiers that men must not be allowed to think,
  as they have their officers to think for them (P1C8). The sergeant-major of
  the regiment used to tell Švejk and other soldiers about vicious circles
  (P1C10). Lt. Lukash applied to transfer to the 91#super[st] two years prior
  to receiving colonel von Zillergut's dog stolen by Švejk (P1C13).
]
== A

#addentry[Alexander The Great][Person][A Macedonian conqueror and hero.  Had a
batman (P1C12).]

#addentry[Anarchist][Person][Mentioned in a conversation about the murder of
Archduke Ferdinand as one of the possible assassins (P1C1).]

#addentry[Angels][Person][Supernatural creatures attending God (P1C1).]

#addentry[Anheim von Kahlsberg][Animal][A fictitious dog, supposedly the father
of Lux the Pomeranian. It is to be entered into Lux' fake pedigree (P1C12).]

#addentry[Archduke Ferdinand][Person][q.v. #emph[Ferdinand\textsubscript[1]].]

#addentry[Archduchess][Person][The wife of Ferdinand, the Archduke. Killed in
Sarajevo, together with her husband, the Archduke Ferdinand (P1C1).]

#addentry[Argentina][Place][A country. Mr. Katz' partner, of Katz and Company,
fled to Argentina after Otto Katz drove the company into bankruptcy (P1C9).]

#addentry[Army Medical Board][Organisation][A medical committee responsible for
evaluating the health of soldiers. Finds Švejk to be an imbecile (P1C1).]

#addentry[Arrestantenbuch][Print][Švejk's arrest documents, carried by the
police officer (P1C5).]

#addentry[Arse][Pejorative][The rear end of many mammals. One of Palivec's
favourite curses (P1C1).]

#addentry[Arsenic][Medicine][The thin as a rake man tries to simulate an illness
that would allow him to avoid military service using arsenic, among other
methods (P1C8).]

#addentry[Asylum Porter][Person][Calls a police officer to escort Švejk out of
the asylum after the latter refuses to leave without being given lunch first
(P1C4).]

#addentry[Aspirin][Medicine][In the military prison, aspirin is a part of the
lightest mode of treatment of malingerers (P1C8).]

#addentry[Asthma][Affliction][A respiratory disease. Kovarik, one of the
malingerer in the military prison suffers from asthma, but Dr. Greunstein's
treatment, together with his speech on malingering, helps cure it (P1C8).]

#addentry[Austria][Place][A country. Did not help Turks against Bulgaria,
Greece and Serbia, which caused the Turks to assassinate Archduke Ferdinand ---
a theory expounded by Švejk in a conversation with Bretschneider (P1C1). The
fate of Austria depends on the clysters given to Švejk by the myrmidon in the
military prison (P1C8). Švejk and the orderly from the barracks think that
Austria is going to get the worst of it (P1C13).]

#addentry[Austria-Hungary][Place][Occupied Bosnia-Herzegovina (P1C1). All the
nations of Austro-Hungary are expected to show devotion and fidelity (P1C7).]

#addentry[Austrian Ministry of War][Organisation][The imperial department
responsible for the mismanagement of the war effort. Decides to call Švejk up
for military service (P1C7).]

#addentry[Axe][Weapon][A pig-gelder from Vodnany uses an axe to kill his
wife (P1C1).] Q.q.v. #emph[Widow], #emph[Pig-gelder].

== B

== C

== D
