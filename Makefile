SRC=*.tex

DVI=$(SRC:.tex=.dvi)
PDF=$(SRC:.tex=.pdf)

BASECLEAN=*.gz *.ind *.idx *.ilg *.bak *.out *.log *.toc *.aux
BASECLEAN+=*.hst *.lof *.ver *.4ct *.4tc *.idv *.lg *.tmp *.xref
BASECLEAN+=*.fls *.fdb_*

PRINTCLEAN=$(PDF) $(RTF) $(DVI) $(HTML) *.css *.svg

all: dvi pdf

pdf: $(PDF)

dvi: $(DVI)

$(DVI): $(SRC)
	latex $<
	latex $<

$(PDF): $(SRC)
	pdflatex $<
	pdflatex $<

.PHONY: clean
clean:
	rm -f $(BASECLEAN)

.PHONY: veryclean
veryclean:
	rm -f $(BASECLEAN) $(PRINTCLEAN)
